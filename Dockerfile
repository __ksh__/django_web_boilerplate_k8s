FROM library/python:3.7-stretch
MAINTAINER __ksh__

RUN apt-get update -y && apt-get install -y less build-essential python-dev curl \
&& rm -rf /var/lib/apt/lists/*
RUN pip install --upgrade pip
RUN pip install pipenv
RUN mkdir -p /etc/uwsgi /app
WORKDIR /app
COPY . .
RUN pipenv install --system
COPY ./uwsgi.yml /etc/uwsgi/uwsgi.yml
EXPOSE 8000
CMD ["uwsgi", "-y", "/etc/uwsgi/uwsgi.yml"]
