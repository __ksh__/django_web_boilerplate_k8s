#!/bin/sh
set -e
apk --update add openjdk7-jre
gcloud components install kubectl
echo -n $GOOGLE_CREDENTIALS > /google_credentials.json
gcloud auth activate-service-account --key-file=/google_credentials.json
gcloud container clusters get-credentials test-cluster-1 --zone asia-northeast1-b
kubectl -n restaur set image deploy/django-${DRONE_BRANCH} django=asia.gcr.io/restaur-20180518/test/django/${DRONE_BRANCH}:${DRONE_COMMIT}