# Django project on k8s.
- Staticfiles collect to Google Cloud Storage.
- Use Cloud SQL.

* tips *
```
# create base64 string then use this command.
echo -n ${PLAIN_TEXT}|base64

# Setting the namespace preference
kubectl config set-context $(kubectl config current-context) --namespace={namespace}

# Generate secret file
kubectl create secret generic {metadata.name} --from-file={key}={value}

# Generate secret literal
kubectl create secret generic {metadata.name} --from-literal={key}={value} --from-lite....

# Update deployment container image
kubectl set image {deploy/deployment.name} {container.name}={gcr.image.path}

# Rollout previous revision
kubectl rollout undo {deploy/deployment.name}

# Migrating workloads to different machine types.
gcloud container node-pools list --cluster {cluster.name}
gcloud container node-pools create {new-node-pool.name} --cluster {cluster.name} [op] [eg: --machine-type=n1-highcpu-2 --disk-size=20 --num-nodes=3 --preemptible]
kubectl get pods -o=wide
kubectl get nodes -l cloud.google.com/gke-nodepool={node-pool.name}
for node in $(kubectl get nodes -l cloud.google.com/gke-nodepool={ex-node-pool.name} -o=name); do kubectl cordon "$node"; done
kubectl get nodes
for node in $(kubectl get nodes -l cloud.google.com/gke-nodepool={ex-node-pool.name} -o=name); do
  kubectl drain --force --ignore-daemonsets --delete-local-data --grace-period=10 "$node";
done
kubectl get pods -o=wide
gcloud container node-pools delete {ex-node-pool.name} --cluster {cluster.name}
gcloud container node-pools list --cluster {cluster.name}
```