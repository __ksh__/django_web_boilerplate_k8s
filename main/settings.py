import os

from google.oauth2 import service_account


BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
DEBUG = bool(os.environ.get('DEBUG', 0))

SECRET_KEY = os.environ.get(
    'SECRET_KEY', 'ko2p811qpvt_*u9=uu6uo@-6yy%3$ti1$9!u3jui&*=ff3&mt!')
ALLOWED_HOSTS = ['*']

# MARK: plugins
INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django_extensions',
    'storages',
    'rest_framework',
]

# MARK: middleware
MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'django.middleware.locale.LocaleMiddleware',
]

# MARK: router
ROOT_URLCONF = 'main.urls'
WSGI_APPLICATION = 'main.wsgi.application'

# MARK: template
TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [os.path.join(BASE_DIR, 'templates')],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.template.context_processors.i18n',
                'django.template.context_processors.media',
                'django.template.context_processors.static',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

# MARK: db
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': os.environ.get('DATABASE_NAME', "restaur"),
        'USER': os.environ.get('DATABASE_USER', "postgres"),
        'PASSWORD': os.environ.get('DATABASE_PASSWORD', "passw0rd"),
        'HOST': os.environ.get('DATABASE_HOST', "127.0.0.1"),
    }
}

# MARK: hash
PASSWORD_HASHERS = [
    'django.contrib.auth.hashers.BCryptPasswordHasher',
    'django.contrib.auth.hashers.BCryptSHA256PasswordHasher',
    'django.contrib.auth.hashers.PBKDF2PasswordHasher',
    'django.contrib.auth.hashers.PBKDF2SHA1PasswordHasher',
]

# MARK: auth
AUTH_PASSWORD_VALIDATORS = [
    {'NAME': 'django.contrib.auth.password_validation\
.UserAttributeSimilarityValidator'},
    {'NAME': 'django.contrib.auth.password_validation\
.MinimumLengthValidator'},
    {'NAME': 'django.contrib.auth\
.password_validation.CommonPasswordValidator'},
    {'NAME': 'django.contrib.auth\
.password_validation.NumericPasswordValidator'},
]

# MARK: i18n
LANGUAGE_CODE = 'ja'
TIME_ZONE = 'Asia/Tokyo'
USE_I18N = True
USE_L10N = True
USE_TZ = True

# MARK: static
GS_BUCKET_NAME = os.environ.get('GS_BUCKET_NAME', "GS_BUCKET_NAME")
GS_CREDENTIALS = os.environ.get('GS_CREDENTIALS', "GS_CREDENTIALS")
DEFAULT_FILE_STORAGE = 'storages.backends.gcloud.GoogleCloudStorage'
THUMBNAIL_FORCE_OVERWRITE = True
STATIC_URL = '/static/'
STATIC_ROOT = os.path.join(BASE_DIR, 'static')
MEDIA_URL = '/media/'
MEDIA_ROOT = os.path.join(BASE_DIR, 'media')

# MARK: logging
DATADOG_TRACE = {
    'AGENT_HOSTNAME': os.environ.get('DATADOG_AGENT_SERVICE_HOST'),
    'ENABLED': (DEBUG is False)
}
LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'handlers': {
        'console': {
            'class': 'logging.StreamHandler',
        }
    },
    'loggers': {
        'main': {
            'handlers': ['console'],
            'level': 'INFO',
        },
        'plugins.app': {
            'handlers': ['console'],
            'level': 'INFO',
        },
    },
}
